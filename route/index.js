


const Router = require('koa-router');
const router = new Router();
const API = require('../lib/api');
const Utils = require('../lib/utils');

router.get('/', async (ctx, next) => {

    await ctx.render('index', { overview});
});

router.get('/alias', async (ctx, next) => {
    await ctx.render('alias', { overview});
});
router.get('/alias/:address', async (ctx, next) => {
    await ctx.render('alias', { overview, address: ctx.params.address});
});

router.get('/:keyword', async (ctx, next) => {

    let keyword = ctx.params.keyword;

    if(/\d+/.test(keyword)){
        await ctx.render('block', { overview, height: keyword});
    }

    if(['about', 'recent_blocks', 'recent_transactions','top_accounts', 'dashboard'].indexOf(keyword) !== -1){

        let ret = { overview};
        ret[keyword] = true;

        await ctx.render(keyword, ret);
    }

    if(keyword.length == 67 && /^blacknet[a-zA-Z0-9]{59}$/.test(keyword)){
        let address = keyword;
        let type = ctx.query.type || 'all';
        await ctx.render('account', { overview, address, type});
    }

    if(keyword.length == 64){

        let instance = await Block.findOne({ blockHash: keyword.toUpperCase() });
        if(instance){
            await ctx.render('block', { overview, height: instance.height});
        } else{
            instance = await Transaction.findOne({ txid: keyword.toUpperCase() });
            await ctx.render('tx', { overview});
        }
    }
    await next();
});

router.get('/api', async (ctx, next) => {

    
    await ctx.render('apis', { apis:true, overview});
});


router.get('/recent_blocks', async (ctx, next) => {

    
    await ctx.render('recent_blocks', { recent: true ,overview});
});

router.get('/recent_transactions', async (ctx, next) => {

    await ctx.render('recent_transactions', { recent_transactions: true ,overview});
});


router.get('/tx/:hash', async (ctx, next) => {

    await ctx.render('tx', { overview});
});

router.get('/search', async (ctx, next) => {
    let key = ctx.query.q;
    if(Utils.verifyAccount(key)){
        return ctx.redirect('/'+ key);
    }
    if(Utils.verifyBlockNumber(key)){
        return ctx.redirect('/'+ key);
    }
    if(Utils.verifyHash(key)){
        let block = await Block.findOne({ blockHash: key.toUpperCase() });
        if(block){
            return ctx.redirect('/block/'+ key);
        }
        let tx = await Transaction.findOne({ txid: key.toUpperCase() });
        if(tx){
            return ctx.redirect('/tx/'+ key);
        }
    }
    next()
});


router.get('/top_accounts', async (ctx, next) => {
    let order = ctx.query.order_by || 'rich';

    await ctx.render('top_accounts', { top_accounts: true ,overview, order});
});

router.get('/block/:height', async (ctx, next) => {

    let height = ctx.params.height;
    await ctx.render('block', { overview, height});
});

router.get('/dashboard', async (ctx, next) => {


    let peers = await Peer.find({});
    let currentPeers = await API.getPeerInfo();


    for (var cpeer of currentPeers) {

        let index = cpeer.remoteAddress.lastIndexOf(':');
        let ip = cpeer.remoteAddress.slice(0, index);
        let dpeer = await Peer.findOne({ ip });

        cpeer.ip = ip;
        cpeer.port = cpeer.remoteAddress.slice(index + 1);
        if (dpeer) {
            cpeer.location = dpeer.location;
        } else {
            cpeer.location = {};
        }

    }

    await ctx.render('dashboard', { currentPeers, peers, dashboard: true ,overview});
});

router.get('/account/:address', async (ctx, next) => {
    let address = ctx.params.address;
    let type = ctx.query.type || 'all';
    await ctx.render('account', { overview, address, type});
});

router.get('/api/account/richlist', async (ctx, next) => {

    let blocks = await Account.find({}).limit(50).sort({ balance: 'desc' });

    ctx.body = blocks;
});


module.exports = router;

