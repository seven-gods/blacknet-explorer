
global.config = require('../config');

const API = require('./api');
const Bignumber = require('bignumber.js');
const IPGeolocationAPI = require('ip-geolocation-api-javascript-sdk');
const ipgeolocationApi = new IPGeolocationAPI(config.ipgeolocation_key, false);
var GeolocationParams = require('ip-geolocation-api-javascript-sdk/GeolocationParams.js');

setInterval(initOverview, 60 * 1000);

async function initOverview() {
    let ledger = await API.getInfo();
    let nodeInfo = await API.getNodeInfo();
    ledger.supply = new Bignumber(ledger.supply).dividedBy(1e8).toFixed(0);
    global.overview = {
        ...ledger,
        ...nodeInfo
    };
}

initOverview();


function unix_to_local_time(unix_timestamp) {

    let date = new Date(unix_timestamp * 1000);
    let hours = "0" + date.getHours();
    let minutes = "0" + date.getMinutes();
    let seconds = "0" + date.getSeconds();
    let day = date.getDate();
    let year = date.getFullYear();
    let month = date.getMonth() + 1;

    return year + "-" + ('0' + month).substr(-2) + "-" +
        ('0' + day).substr(-2) + " " + hours.substr(-2) + ':'
         + minutes.substr(-2) + ':' + seconds.substr(-2);
}



setInterval(ipLocation, 1000 * 60 * 60);
ipLocation();
async function ipLocation() {

    let peerdb = await API.getPeerDB();
    let peers = peerdb.peers;

    for (let peer of peers) {

        let index = peer.lastIndexOf(':'), itslimit;
        let ip = peer.slice(0, index).replace(/[\[\]]/g, '');

        let p = await Peer.findOne({ ip });
        if(p){
            itslimit = p.location.message == 
            "You have exceeded your subscription's requests limit of 1000 per day.";
        }
        if (p && !itslimit) continue;

        if (!p) {
            p = { ip, port: peer.slice(index + 1) };
        }

        p.location = await getLocation(ip);

        if (!itslimit) {
            p = new Peer(p);
        }
        await p.save();
    }
}

function getLocation(ip) {

    return new Promise((resolve, reject) => {
        var geolocationParams = new GeolocationParams();
        geolocationParams.setIPAddress(ip);
        geolocationParams.setFields("geo,time_zone,currency");
        ipgeolocationApi.getGeolocation(function (json) {
            resolve(json);
        }, geolocationParams);
    })
}
