

const defaultSupply = 10e8;
const API = require('./api');
let startHeight = 0;
let blankSign = new Array(128).join('0');

async function process() {

    let blockHash, block, dbbk, supply = defaultSupply;
    let lastBlock = await Block.findOne({}).sort({ height: 'desc' });;

    if (lastBlock) {
        startHeight = lastBlock.height;
        supply = getSupplyWithHeight(startHeight);
    }

    console.log('Syncing start', startHeight)
    console.time('Sync')
    while (++startHeight) {

        blockHash = await API.getBlockHash(startHeight);
        if (blockHash == 'block not found'){
            console.timeEnd('Sync')
            break;
        }

        block = await API.getBlock(blockHash, true);

        if (block == 'invalid hash'){
            console.timeEnd('Sync')
            break;
        }

        dbbk = await Block.findOne({ blockHash });

        if (!dbbk) {
            dbbk = new Block(getBlock(block, startHeight, blockHash));
            dbbk.blockHash = blockHash;
            dbbk.timestr = unix_to_local_time(dbbk.time);

            if (block && block.transactions.length > 0) {
                txSave(dbbk);
            }
        }

        let reward = getReward(supply);

        supply += reward;

        global.Supply = supply

        let blockReward = reward.toFixed(9).slice(0,-1);

        let fees = getFees(dbbk.transactions),
            fee = fees + reward * 1e8;

        let tx = {
            size: 142,
            signature: blankSign,
            from: block.generator,
            seq: 0,
            blockHash: blockHash,
            fee: fees,
            type: 254,
            time: unix_to_local_time(block.time),
            data: {
                blockHeight: startHeight,
                blockHash,
                amount: fee
            }
        }
        tx = new Transaction(tx);
        tx.save();
        updateBalance(tx.from, 1);
        dbbk.reward = blockReward;
        dbbk.save();
        if(startHeight%1000 == 0){
            console.log(startHeight)
            console.timeEnd('Sync')
            console.time('Sync')
        }
    }

    setTimeout(process, 2000);
}

process();


async function updateBalance(address, isGenerator) {

    let account = await Account.findOne({ address });

    let data = await API.getBalance(address);
    if (!account) {
        
        let instance = {
            address,
            txamount: 1,
            blocks: 0
        };
        account = new Account(instance);
    }

    if(isNaN(data.balance) == false){
        account.balance = data.balance;
        account.confirmedBalance = data.confirmedBalance;
        account.stakingBalance = data.stakingBalance;
        if(isGenerator) account.blocks += 1;

        await account.save();
    }
}


async function txSave(block) {

    let accountMap = {};

    for (let tx of block.transactions) {

        let dbtx;

        tx.data = JSON.parse(tx.data);
        dbtx = await Transaction.findOne({ txid: tx.hash });

        if (dbtx == null) {

            tx = getTransaction(tx, block.blockHash, block.height);
            tx = new Transaction(tx);
            tx.time = block.time;
            await tx.save();

            accountMap[tx.from] = true;
            accountMap[tx.to] = true;

            await updateLeaseBalance(tx);
            
        } else {
            console.log(dbtx.txid + ' is exist');
        }
    }

    for(let address in accountMap){
        await updateBalance(address);
    }
}

function getBlock(data, height, blockHash) {
    let block = {};

    block.size = data.size;
    block.blockHash = blockHash;
    block.height = height;
    block.version = data.version;
    block.previous = data.previous;
    block.time = unix_to_local_time(data.time);
    block.generator = data.generator;
    block.contentHash = data.contentHash;
    block.signature = data.signature;
    block.transactions = data.transactions.map((tx) => JSON.parse(tx));
    return block;
}

function getTransaction(json, blockHash, blockHeight) {
    let tx = {};

    tx.txid = json.hash;
    tx.size = json.size;
    tx.signature = json.signature;
    tx.from = json.from;
    tx.seq = json.seq;
    tx.blockHash = blockHash;
    tx.fee = json.fee;
    tx.type = json.type;
    tx.data = json.data;
    tx.to = json.data.to;
    tx.amount = json.data.amount;
    tx.data.blockHeight = blockHeight

    return tx;
}



function unix_to_local_time(unix_timestamp) {

    // let date = new Date(unix_timestamp * 1000);
    // let hours = "0" + date.getHours();
    // let minutes = "0" + date.getMinutes();
    // let seconds = "0" + date.getSeconds();
    // let day = date.getDate();
    // let year = date.getFullYear();
    // let month = date.getMonth() + 1;

    // return year + "-" + ('0' + month).substr(-2) + "-" +
    //     ('0' + day).substr(-2) + " " + hours.substr(-2) + ':'
    //      + minutes.substr(-2) + ':' + seconds.substr(-2);
    return unix_timestamp;
}


function getSupplyWithHeight(height){

    let supply = defaultSupply, start = 0;
    
    if(height == start) return supply;

    while(start++ < height){

        supply += getReward(supply);
    }

    return supply;
}

function getReward(supply) {
    let magicNumber = 492750 * 100;
    return supply/magicNumber;
}

function getFees(txns) {
    let fees = 0;
    for (let tx of txns) {
        fees += tx.fee;
    }
    return fees;
}


async function updateLeaseBalance(tx) {

    let fromAccount, toAcccount;
    if(!tx) return;

    fromAccount = await Account.findOne({address: tx.from});
    toAcccount  = await Account.findOne({address: tx.to});

    if(!fromAccount || !toAcccount) return;
    
    if(tx.type == 2){
        
        fromAccount.outLeasesBalance += tx.data.amount;
        toAcccount.inLeasesBalance += tx.data.amount;
        fromAccount.realBalance = fromAccount.outLeasesBalance + fromAccount.balance;
    }

    if(tx.type == 3){
        
        fromAccount.outLeasesBalance -= tx.data.amount;
        toAcccount.inLeasesBalance -= tx.data.amount;
        fromAccount.realBalance = toAcccount.outLeasesBalance + toAcccount.balance;
    }

    if(fromAccount.realBalance == 0){
        fromAccount.realBalance = fromAccount.balance;
    }

    if(toAcccount.realBalance == 0){
        toAcccount.realBalance = toAcccount.balance;
    }


    await fromAccount.save();
    await toAcccount.save();
}
